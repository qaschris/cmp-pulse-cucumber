
Feature: Kid eats a cucumber

Scenario Outline: eating cucumbers
  Given there are <start> cucumbers
  When I eat <eat> cucumbers
  Then I should have <left> cucumbers

  Examples:
    | start | eat | left |
    |    12 |   5 |    7 |
    |    20 |   5 |   15 |
    |    40 |  10 |   30 |
    |     1 |   1 |    0 |